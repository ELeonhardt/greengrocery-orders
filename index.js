const form = document.querySelector('.nancy-form');
const textarea = document.querySelector('.modal-inner textarea');
const modalOuter = document.querySelector('.modal-outer');


const labels = [
    { type: 'number', label: 'zanahoria', text: 'kg de Zanahoria', default: 3},
    { type: 'number', label: 'tomate', text: 'kg de Tomate', default: 2},
    { type: 'number', label: 'papa', text: 'kg de Papa', default: 3},
    { type: 'number', label: 'batata', text: 'kg de Batata', default: 3},
    { type: 'number', label: 'pepinos', text: 'uds de Pepino', default: 6},
    { type: 'number', label: 'zuccinis', text: 'uds de Zuccini', default: 6},
    { type: 'number', label: 'chauchas', text: 'kg de Chauchas', default: 0.5},
    { type: 'number', label: 'manzana-verde', text: 'kg de Manzana Verde', default: 1},
    { type: 'number', label: 'limón', text: 'kg de Limon', default: 3},
    { type: 'number', label: 'cebolla', text: 'kg de cebolla', default: 2},
    { type: 'number', label: 'cebolla-morada', text: 'kg de Cebolla Morada', default: 0.5},
    { type: 'number', label: 'banana', text: 'uds de Banana', default: 5},
    { type: 'number', label: 'remolacha', text: 'cabezas de remolacha', default: 8},
    { type: 'number', label: 'palta', text: 'uds de Palta', default: 8},
    { type: 'number', label: 'morron-rojo', text: 'uds de Morron rojo', default: 4},
    { type: 'number', label: 'morron-verde', text: 'uds de Morron verde', default: 4},
    { type: 'number', label: 'ajo', text: 'uds de cabezas de ajo', default: 3},
    { type: 'number', label: 'rabanitos', text: 'atado de Rabanitos', default: 1},
    { type: 'number', label: 'lechuga', text: 'plantines de lechuga', default: 1},
    { type: 'number', label: 'rucula', text: 'paquetes de rúcula', default: 1},
    { type: 'number', label: 'espinaca', text: 'paquetes de espinaca', default: 1},
    { type: 'number', label: 'zapallo', text: 'Zapallo', default: 1},
    { type: 'number', label: 'repollo-morado', text: 'Cabeza de Repollo Morado', default: 1},
    { type: 'number', label: 'repollo-blanco', text: 'Cabeza de Repollo Blanco', default: 1},
    { type: 'number', label: 'hongos', text: 'g de Champignones', default: 500},
    { type: 'text', label: 'perejil', text: 'Perejil', default: 'algo de'},
]

function createForm(options) {
    const text1 = `<textarea class="otras-opciones" cols="50" rows="3">Hola Nancy, aca te mando mi pedido:</textarea>`;
    const text2 = `<textarea class="otras-opciones" cols="25" rows="3">Lo vengo a buscar a las 19hs. Un abrazo!</textarea>`;
    const inputs =  options.map( (option) => {
        return `
        <div class="item">
            <input type="${option.type}" name="${option.label}" id="${option.label}" value="${option.default}">
            <label for="${option.label}">${option.text}</label>
            <button class="cancel">X</button>
        </div>`;
    }).join('');
    return `${text1}${inputs}${text2}`;
}

function createText(event) {
    event.preventDefault();
    if(event.submitter.matches('button.cancel')) {
        return false;
    }
    const text = document.querySelectorAll('.otras-opciones');
    const inputs = Array.from(event.target.querySelectorAll('input'));
    console.log(inputs);
    const output = inputs.map(input => {
        return `${input.value} ${input.computedName || input.nextElementSibling.innerText}`;
    }).join('\n');
    const finalText = `${text[0].value}\n${output}\n${text[1].value}`;
    textarea.textContent = finalText;
    modalOuter.classList.add('open');

}

function removeElement(event) {
        // find the parent:
        let parent = event.currentTarget.closest('.item');
        parent.remove();
        parent = null; //prevent memory leak
}

function closeModal(event) {
    if(event.target.closest('.modal-inner')) {
        console.log('click inside');
    }
    else {
        modalOuter.classList.remove('open');
    }
}

const html = createForm(labels);
form.insertAdjacentHTML('afterbegin', html);
const cancelButtons = document.querySelectorAll('.cancel');
cancelButtons.forEach(button => button.addEventListener('click', removeElement))
form.addEventListener('submit', createText);
modalOuter.addEventListener('click', closeModal);